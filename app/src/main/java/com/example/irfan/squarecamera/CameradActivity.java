package com.example.irfan.squarecamera;

import android.app.AlertDialog;
import android.app.VoiceInteractor;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.Date;
import java.util.concurrent.TimeUnit;


public class CameradActivity extends AppCompatActivity {
    databaseHelper myDB;
    private CameraView camerad;
    private CameraKitEventListener cameradListener;
    private Button btnCapture;
    private TextView textView;
    private Integer counter = 0;
    long lStartTime;
    long lEndTime;
    private String hintTidakPakaiKacamata[] = new String[]{
            "1. Normal tegak lurus kamera.",
            "2. Normal tegak lurus kamera berkacamata.",
            "3. Tersenyum tegak lurus kamera.",
            "4. Sedih tegak lurus kamera.",
            "5. Mengantuk tegak lurus kamera.",
            "6. Normal menoleh ke kanan 30 derajat.",
            "7. Normal menoleh ke kanan 30 derajat beracamata.",
            "8. Tersenyum menoleh ke kanan 30 derajat.",
            "9. Sedih menoleh ke kanan 30 derajat.",
            "10. Mengantuk menoleh ke kanan 30 derajat.",
            "11. Normal menoleh ke kiri 30 derajat.",
            "12. Normal menoleh ke kiri 30 derajat berkacamata.",
            "13. Tersenyum menoleh ke kiri 30 derajat.",
            "14. Sedih menoleh ke kiri 30 derajat.",
            "15. Mengantuk menoleh ke kiri 30 derajat.",
            "16. Normal tegak lurus kamera muka basah.",
            "17. Normal tegak lurus kamera berkacamata muka basah.",
            "18. Tersenyum tegak lurus kamera muka basah.",
            "19. Sedih tegak lurus kamera muka basah.",
            "20. Mengantuk tegak lurus kamera muka basah.",
            "21. Normal menoleh ke kanan 30 derajat muka basah.",
            "22. Normal menoleh ke kanan 30 derajat berkacamata muka basah.",
            "23. Tersenyum menoleh ke kanan 30 derajat muka basah.",
            "24. Sedih menoleh ke kanan 30 derajat muka basah",
            "25. Mengantuk menoleh ke kanan 30 derajat muka basah",
            "26. Normal menoleh ke kiri 30 derajat muka basah",
            "27. Normal menoleh ke kiri 30 derajat berkacamata muka basah.",
            "28. Tersenyum menoleh ke kiri 30 derajat muka basah.",
            "29. Sedih menoleh ke kiri 30 derajat muka basah.",
            "30. Mengantuk menoleh ke kiri 30 derajat muka basah."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camerad);
        final Intent intent = getIntent();
        myDB = new databaseHelper(this);
        cameradListener = new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {

            }

            @Override
            public void onError(CameraKitError cameraKitError) {

            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {
                byte[] picture = cameraKitImage.getJpeg();
                Bitmap result = BitmapFactory.decodeByteArray(picture, 0, picture.length);
                result = Bitmap.createScaledBitmap(result, 512,512, true);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                String myBase64Image = encodeToBase64(result, Bitmap.CompressFormat.JPEG, 100);
                final ApiInterface api = Server.getclient().create(ApiInterface.class);
                Log.d("test", "onImage: "+myBase64Image);
                JSONObject paramObject = new JSONObject();

                lStartTime = new Date().getTime();
                Call<ResponseApi> kirim =api.kirim(intent.getStringExtra("nrp"),"data:image/jpeg;base64,"+myBase64Image);
                kirim.enqueue(new Callback<ResponseApi>() {
                    @Override
                    public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                        lEndTime = new Date().getTime();
                        Toast.makeText(CameradActivity.this, "berhasil", Toast.LENGTH_SHORT).show();

                        long outputime = lEndTime - lStartTime;
                        boolean isinserted = myDB.insertData(intent.getStringExtra("nrp") , counter, outputime);
                        if (isinserted == true){
                            Toast.makeText(CameradActivity.this, "Data Inserted", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(CameradActivity.this, "NOT Inserted", Toast.LENGTH_SHORT).show();
                        }
                        counter++;
                        if (counter >= 2 ){
                            Cursor res = myDB.getAllData();
                            Integer total = 0, count = 0;
//                            Cursor total = myDB.getAverage();

                            if (res.getCount()==0){
                                //write message
                                showMessage("ERROR!", "there is no data here");
                                return;
                            }
                            StringBuffer buffer = new StringBuffer();
                            while(res.moveToNext()){
                                buffer.append("nrp : "+ res.getString(0)+"\n");
                                buffer.append("count : "+ res.getString(1)+"\n");
                                buffer.append("time " + res.getString(2)+"\n\n\n");
                                total += Integer.parseInt(res.getString(2));
                                count++;
                            }
                            Double average = (double)(total / count);
                            buffer.append("Average : "+ average +"\n");

                            showMessage("Data", buffer.toString());

                            myDB.resetAllData();
                            Toast.makeText(CameradActivity.this,"show all data", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(CameradActivity.this, MainActivity.class);
//                            startActivity(intent);

//                            onPause();
                        }
                        textView.setText("HINT: " + hintTidakPakaiKacamata[counter]);
                    }

                    @Override
                    public void onFailure(Call<ResponseApi> call, Throwable t) {
                        t.printStackTrace();
                        Toast.makeText(CameradActivity.this, "anjir", Toast.LENGTH_SHORT).show();
                    }
                });


            }

            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        };

        textView = findViewById(R.id.hintView);
        textView.setText("HINT: " + hintTidakPakaiKacamata[0]);

        camerad = (CameraView) findViewById(R.id.camerad);
        camerad.addCameraKitListener(cameradListener);

        btnCapture = (Button) findViewById(R.id.btn_capture);
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camerad.captureImage();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        camerad.start();
    }

    @Override
    protected void onPause() {
        camerad.stop();
        super.onPause();
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

}
