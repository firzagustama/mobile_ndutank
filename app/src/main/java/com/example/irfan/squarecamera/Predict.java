package com.example.irfan.squarecamera;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Predict {
    @FormUrlEncoded
    @POST("/predict")
    Call<ResponseApi> kirim (@Field("nrp") String nrp,
                             @Field("imagefile") String image);
}
